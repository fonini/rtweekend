#!/usr/bin/env python3

# In order:
# ./do-the-thing.py --help
# ./do-the-thing.py config
# ./do-the-thing.py build
# ./do-the-thing.py

# https://raytracing.github.io/books/RayTracingInOneWeekend.html

from __future__ import annotations

import argparse
import datetime
import os
import pathlib
import re
import shutil
import subprocess
import sys
import tempfile
import time
from typing import Any, Literal, Optional, Union, IO

from colorama import Fore, Style, init

assert sys.version_info >= (3, 9)


default_png_filename = "out.png"

shlex_find_unsafe = \
    re.compile(r'[^\w@%+=:,./-]', re.ASCII).search
shlex_find_unsafe_and_not_tick_or_space = \
    re.compile(r"[^\w@%+=:,./ -']", re.ASCII).search
shlex_substs = {
    '\n': r"\n",
    '\033': r"\e",
    '\r': r"\r",
    '\t': r"\t",
    '\\': r"\\",
    '\'': r"\'",
}


def shlex_check_single_line_tick_space(s: str) -> bool:
    if len(s.split('\n')) > 1:
        return False
    if shlex_find_unsafe_and_not_tick_or_space(s):
        return False
    return True


def shlex_quote(s: str, *, _empty_is_safe: bool = False) -> str:
    if not s:
        if _empty_is_safe:
            return ""
        else:
            return "''"

    if shlex_find_unsafe(s) is None:
        return s

    seq_of_tick = True
    for c in s:
        if c != "'":
            seq_of_tick = False
    if seq_of_tick:
        return "\\'" * len(s)

    if shlex_check_single_line_tick_space(s):
        return '"' + s + '"'

    blocks = re.split(r"('+)", s)

    if len(blocks) > 1:
        i = 0
        while i < len(blocks):  # join adjacent blocks if they're simple
            if shlex_check_single_line_tick_space(blocks[i]):
                while (
                    i + 1 < len(blocks)
                    and shlex_check_single_line_tick_space(blocks[i + 1])
                ):
                    # join blocks i and i+1
                    blocks[i] += blocks[i + 1]
                    del blocks[i + 1]
            i += 1
        return ''.join(shlex_quote(b, _empty_is_safe=True) for b in blocks)

    for c in s:
        assert c != "'"  # seq_of_tick must be false in this region of code

    use_dollar_tick = False
    for c in ['\n', '\033', '\r', '\t']:
        if c in s:
            use_dollar_tick = True

    if not use_dollar_tick:
        return "'" + s + "'"

    output = "$'"
    for c in s:
        if c in shlex_substs:
            output += shlex_substs[c]
        else:
            output += c
    output += "'"
    return output


def shlex_join(split_command: list[str]) -> str:
    return ' '.join(shlex_quote(arg) for arg in split_command)


class LazyChoicesAction(argparse.Action):
    def __init__(self,
                 option_strings,
                 dest,
                 nargs=None,
                 const=None,
                 default=None,
                 type=None,
                 choices=None,
                 required=False,
                 help=None,
                 metavar=None):
        assert nargs is None
        assert const is None
        assert choices is not None
        assert len(choices) >= 1
        if default is None:
            default = choices[0]
        self.__choices = choices
        super().__init__(
            option_strings=option_strings,
            dest=dest,
            nargs='?',
            const=const,
            default=default,
            type=type,
            choices=None,
            required=required,
            help=help,
            metavar=metavar)

    def __call__(self, parser, namespace, values, option_string=None):
        val = values.lower()
        for c in self.__choices:
            if c.lower().startswith(val):
                setattr(namespace, self.dest, c)
                return
        choices_str = ', '.join(map(repr, self.__choices))
        msg = f"invalid choice: {val!r} (choose from {choices_str})"
        raise argparse.ArgumentError(self, msg)


def announce(
    title: str,
    txt: Union[str, pathlib.Path],
    file=sys.stderr,
    **kwargs,
) -> None:
    print(f"{Fore.CYAN}{title}:{Style.RESET_ALL} {txt}",
          file=file, **kwargs)


def announce_err(
    title: str,
    txt: Union[str, pathlib.Path],
    file=sys.stderr,
    **kwargs,
) -> None:
    print(f"{Fore.RED}{Style.BRIGHT}{title}:{Style.RESET_ALL} {txt}",
          file=file, **kwargs)


def guess_kit(
    gcc_version_regex=re.compile(r"gcc.*\b(\d+.\d+.\d+)", re.IGNORECASE)
) -> str:
    # always guesses f"GCC {gcc_version}"
    gcc_completed = subprocess.run(['gcc', '--version'], check=True,
                                   stdout=subprocess.PIPE)
    lines = gcc_completed.stdout.decode().split('\n')

    for line in lines:
        if match := gcc_version_regex.search(line):
            gcc_version = match.group(1)
            break
    else:
        raise RuntimeError("Unable to guess kit name.")

    return f"GCC {gcc_version}"


capitalized_bt = {
    'release': 'Release',
    'debug': 'Debug',
    'relwithdebinfo': 'RelWithDebInfo',
}


def get_build_dir(
    build_type: Literal['release', 'debug', 'relwithdebinfo'],
) -> pathlib.Path:
    build_area = pathlib.Path('build')
    if not build_area.is_dir():
        build_area.mkdir()

    kit_dirs = [k for k in build_area.iterdir() if k.is_dir()]
    if len(kit_dirs) == 0:
        kit_dir = pathlib.Path('build') / guess_kit()
        announce("No building kit directory was found under ‘build/’",
                 f"Creating one at ‘{kit_dir}’.")
        kit_dir.mkdir()
    elif len(kit_dirs) > 1:
        raise RuntimeError(
            "Too many kit-directories found under ‘build/’.")
    else:  # len(kit_dirs) == 1
        kit_dir = kit_dirs[0]

    try:
        dir_name = capitalized_bt[build_type]
    except KeyError:
        raise RuntimeError(f"Unknown BUILD_TYPE ‘{build_type}’")
    return kit_dir / dir_name


###
###     COMMAND: CONFIGURATION
###

def execute_config(
    build_type: Literal['release', 'debug', 'relwithdebinfo'],
    build_dir: pathlib.Path,
    curses_cmake: bool = False,
    extra_args: tuple = (),
) -> None:
    cmake_var_flags: list[str] = []
    if not (build_dir / "CMakeCache.txt").is_file():
        cmake_var_flags = [
            "-DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=ON",
            f"-DCMAKE_BUILD_TYPE:STRING={capitalized_bt[build_type]}",
            "-S", os.fspath(pathlib.Path().resolve()),
            "-G", "Ninja",
        ]
    cmdline = [
        "cmake" if not curses_cmake else "ccmake",
        *cmake_var_flags,
        "-B", os.fspath(build_dir.resolve()),
        *extra_args,
        # "-LH"  # para fazer o CMake listar as variáveis
    ]
    announce("Configuration command", shlex_join(cmdline))
    before = time.monotonic()
    subprocess.run(cmdline, check=True)
    after = time.monotonic()
    if not curses_cmake:
        announce("Time elapsed", f"{after - before:.3g} s")


def config_command(args: argparse.Namespace) -> None:
    build_dir = get_build_dir(args.BUILD_TYPE)
    announce("Build directory", build_dir)
    if not build_dir.is_dir():
        build_dir.mkdir(parents=True)
    else:
        if args.CLEAN_BUILD_DIR:
            announce("Remove tree", build_dir)
            shutil.rmtree(build_dir)
            build_dir.mkdir()
    execute_config(args.BUILD_TYPE, build_dir, args.CCMAKE, args.ARGS)


###
###     COMMAND: BUILD
###

def execute_clean_build_dir(
    build_type: Literal['release', 'debug', 'relwithdebinfo'],
    build_dir: pathlib.Path,
) -> None:
    cmdline = [
        "cmake",
        "--build", os.fspath(build_dir.resolve()),
        "--config", capitalized_bt[build_type],
        "--target", "clean",
    ]
    announce("Clean command", shlex_join(cmdline))
    subprocess.run(cmdline, check=True)


def execute_build(
    build_type: Literal['release', 'debug', 'relwithdebinfo'],
    build_dir: pathlib.Path,
    verbose: bool = False,
) -> None:
    if not build_dir.is_dir():
        announce("Missing build directory", build_dir)
        build_dir.mkdir()
        execute_config(build_type, build_dir)
        print('', file=sys.stderr)
    if verbose:
        verbose_args = ['--verbose']
    else:
        verbose_args = []
    cmdline = [
        "cmake",
        "--build", os.fspath(build_dir.resolve()),
        "--config", capitalized_bt[build_type],
        "--target", "all",
        *verbose_args,
    ]
    cmdline_str = shlex_join(cmdline)
    announce("Build command", cmdline_str)
    reconf_and_try_again = False
    try:
        before = time.monotonic()
        subprocess.run(cmdline, check=True)
        after = time.monotonic()
    except subprocess.CalledProcessError as e:
        announce_err("Build error", str(e))
        print("\nClean, reconfigure and then try again? [y/N] ", end='',
              file=sys.stderr)
        if sys.stdin.readline().lower().startswith('y'):
            reconf_and_try_again = True
        else:
            sys.exit(e.returncode)
    if reconf_and_try_again:
        print("", file=sys.stderr)
        announce("Remove tree", build_dir)
        shutil.rmtree(build_dir)
        build_dir.mkdir()
        print("", file=sys.stderr)
        execute_config(build_type, build_dir)
        print("", file=sys.stderr)
        execute_build(build_type, build_dir, verbose)
    else:
        announce("Time elapsed", f"{after - before:.3g} s")


def build_command(args: argparse.Namespace) -> None:
    build_dir = get_build_dir(args.BUILD_TYPE)
    announce("Build directory", build_dir)
    if args.CLEAN_BUILD_DIR:
        execute_clean_build_dir(args.BUILD_TYPE, build_dir)
    execute_build(args.BUILD_TYPE, build_dir, verbose=args.VERBOSE_MAKE)


###
###     COMMAND: RUN
###

def get_exec_name() -> str:
    regex = re.compile(r"add_executable\s*\(\s*(\w+)", re.IGNORECASE)
    with pathlib.Path("CMakeLists.txt").open() as f:
        for line in f:
            if match := regex.search(line):
                return match.group(1)
    raise RuntimeError("‘add_executable’ not found in CMakeLists.txt.")


def execute_run(
    build_dir: pathlib.Path,
    exec_args: list[str],
    output_behavior: Literal['discard', 'all-pass', 'capture'],
) -> tuple[subprocess.CompletedProcess, float]:
    exec_path = build_dir / get_exec_name()
    cmdline = [os.fspath(exec_path)] + exec_args

    cmdline_str = shlex_join(cmdline)
    run_kwargs: dict[str, Any] = {}
    if output_behavior == 'discard':
        cmdline_str += " >/dev/null 2>&1"
        run_kwargs['stdout'] = subprocess.DEVNULL
        run_kwargs['stderr'] = subprocess.DEVNULL
    elif output_behavior == 'capture':
        cmdline_str += " | ..."
        run_kwargs['stdout'] = subprocess.PIPE
    elif output_behavior == 'all-pass':
        pass
    else:
        raise ValueError("'output_behavior' with unknown value")

    announce("Run command", cmdline_str)

    before = time.monotonic()
    completed_process = subprocess.run(cmdline, check=True, **run_kwargs)
    after = time.monotonic()
    time_rendering = after - before
    announce("Time runnning", f"{time_rendering:.3g} s")

    return completed_process, time_rendering


def git_describe() -> str:
    try:
        describe = subprocess.run(
            ['git', 'describe'],
            capture_output=True,
            text=True,
            check=True,
        ).stdout.strip()
        git_working = True
    except subprocess.CalledProcessError as e:
        announce_err("Git error", f"{e!s}:")
        for line in e.stderr.splitlines():
            announce_err("", line)
        announce_err("Git error", "No version metadata will be recorded.")
        print(file=sys.stderr)
        describe = "<no git version available>"
        git_working = False
    except FileNotFoundError as e:
        announce_err("Git error", f"{e!s}")
        announce_err("Git error", "No version metadata will be recorded.")
        print(file=sys.stderr)
        describe = "<no git version available>"
        git_working = False

    if git_working:
        try:
            status: Optional[str] = subprocess.run(
                ['git', 'status', '--porcelain'],
                capture_output=True,
                text=True,
                check=True,
            ).stdout
        except subprocess.CalledProcessError as e:
            announce_err("Git error", f"{e!s}:")
            for line in e.stderr.splitlines():
                announce_err("", line)
            announce_err(
                "Something wrong's not right",
                "'git status' should have worked. What's going on?")
            print(file=sys.stderr)
            status = None
        except FileNotFoundError as e:
            announce_err("Git error", f"{e!s}")
            announce_err(
                "Something wrong's not right",
                "'git status' should have worked. What's going on?")
            print(file=sys.stderr)
            status = None
    else:
        status = None

    if status:
        describe += " (dirty tree)\n"
        for line in status.splitlines():
            describe += f" {line}\n"

    return describe[:-1] if describe.endswith('\n') else describe


def execute_generate_png(
        build_dir: pathlib.Path,
        ppm_contents: bytes,
        time_rendering: float,
        png_filename: str,
        never_overwrite: bool,
        args: argparse.Namespace,
) -> None:
    png_comment = (
        f"RTWeekend {git_describe()}\n"
        f"Datetime: {datetime.datetime.now()}\n"
        f"Cmdline args: {sys.argv}\n"
        f"Parsed cmdline args: {args!r}\n"
        f"Executable path: {build_dir / get_exec_name()}\n"
        f"Time rendering: {time_rendering:.3g} s\n"
    )

    f: Optional[IO]
    png_file = pathlib.Path(png_filename)
    if never_overwrite:
        try:
            f = png_file.open('x')
        except FileExistsError as e:
            announce_err("PNG file exists", str(e))
            sys.exit(1)
        f.close()

    try:
        f = None
        with tempfile.NamedTemporaryFile('wt', delete=False) as f:
            f.write(png_comment)
        cmdline = ['convert', '-comment', f'@{f.name}',
                   'PPM:-', png_filename]
        announce("Generate PNG", "... | " + shlex_join(cmdline))
        before = time.monotonic()
        subprocess.run(cmdline, check=True, input=ppm_contents)
        after = time.monotonic()
    finally:
        if f is not None:
            pathlib.Path(f.name).unlink(missing_ok=True)
    announce("Time elapsed", f"{after - before:.3g} s")


def run_command(args: argparse.Namespace) -> None:
    build_type = args.BUILD_TYPE if 'BUILD_TYPE' in args else 'release'
    build_dir = get_build_dir(build_type)
    announce("Build directory", build_dir)

    action_on_result = \
        args.ACTION_ON_RESULT \
        if 'ACTION_ON_RESULT' in args else \
        'view image'

    also_build_proj = \
        args.ALSO_BUILD_PROJ if 'ALSO_BUILD_PROJ' in args else True
    if action_on_result == 'print time rendering':
        also_build_proj = False

    if also_build_proj:
        execute_build(build_type, build_dir, verbose=False)
        print(file=sys.stderr)

    png_filename = \
        args.PNG_FILENAME \
        if 'PNG_FILENAME' in args else \
        default_png_filename

    never_overwrite_png = \
        args.NEVER_OVERWRITE_PNG \
        if 'NEVER_OVERWRITE_PNG' in args else \
        False

    exec_args = args.ARGS if 'ARGS' in args else []

    output_behavior: Literal['discard', 'all-pass', 'capture']
    if action_on_result == 'print time rendering':
        output_behavior = 'discard'
    elif action_on_result == 'send to stdout':
        output_behavior = 'all-pass'
    else:  # 'generate png' or 'view image'
        output_behavior = 'capture'

    completed_process, time_rendering = execute_run(
        build_dir, exec_args, output_behavior)

    if action_on_result == 'print time rendering':
        print(time_rendering)  # to stdout
        return

    if action_on_result != 'send to stdout':
        print(file=sys.stderr)
        execute_generate_png(
            build_dir, completed_process.stdout, time_rendering,
            png_filename, never_overwrite_png, args)
        del completed_process

    if action_on_result == 'view image':
        subprocess.run(['feh', '-F', png_filename], check=True)


###
###     COMMAND: DEBUG
###

def execute_debug(build_dir: pathlib.Path,
                  exec_args: list[str]) -> None:
    exec_path = build_dir / get_exec_name()
    cmdline = ['gdb', '--args', os.fspath(exec_path)] + args.ARGS
    announce("Run command", shlex_join(cmdline))
    subprocess.run(cmdline, check=True)


def debug_command(args: argparse.Namespace) -> None:
    build_dir = get_build_dir(args.BUILD_TYPE)
    announce("Build directory", build_dir)
    if args.ALSO_BUILD_PROJ:
        execute_build(args.BUILD_TYPE, build_dir, verbose=False)
        print(file=sys.stderr)
    execute_debug(build_dir, args.ARGS)


###
###     CLI PARSING
###

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Thank you, Zhu Li.")
    subparsers = parser.add_subparsers(title='commands')

    # CONFIG

    parser_config = subparsers.add_parser('config')
    parser_config.set_defaults(func=config_command)

    parser_config.add_argument(
        "-x", "--clean",
        help="Delete the current build directory (if any).",
        action='store_true',
        dest='CLEAN_BUILD_DIR',
    )

    parser_config.add_argument(
        "-c", "--curses",
        help="Summon ccmake",
        action='store_true',
        dest='CCMAKE',
    )

    parser_config.add_argument(
        "BUILD_TYPE",
        help="Release, Debug, or RelWithDebInfo.",
        type=str,
        choices=['release', 'debug', 'relwithdebinfo'],
        action=LazyChoicesAction,
    )

    parser_config.add_argument(
        "ARGS",
        help="Extra CMake options.",
        nargs="*",
    )

    # BUILD

    parser_build = subparsers.add_parser('build')
    parser_build.set_defaults(func=build_command)

    parser_build.add_argument(
        "BUILD_TYPE",
        help="Release, Debug, or RelWithDebInfo.",
        type=str,
        choices=['release', 'debug', 'relwithdebinfo'],
        action=LazyChoicesAction,
    )

    parser_build.add_argument(
        "-x", "--clean",
        help="Clean build directory before rebuild.",
        action='store_true',
        dest='CLEAN_BUILD_DIR',
    )

    parser_build.add_argument(
        "-v", "--verbose",
        help="Force CMake to print the commands it executes.",
        action='store_true',
        dest='VERBOSE_MAKE',
    )

    # RUN

    parser_run = subparsers.add_parser('run')
    parser_run.set_defaults(func=run_command)

    run_group_build_type = parser_run.add_mutually_exclusive_group()

    run_group_build_type.add_argument(
        "-d", "--debug",
        help="Run debug build (default is to run the release build).",
        dest='BUILD_TYPE',
        default='release',
        const='debug',
        action='store_const',
    )

    run_group_build_type.add_argument(
        "-w", "--rel-with-deb",
        help="Run 'Release with debug info' build (default is to run the "
             "release build).",
        dest='BUILD_TYPE',
        default='release',
        const='relwithdebinfo',
        action='store_const',
    )

    run_group_result_action = parser_run.add_mutually_exclusive_group()

    run_group_result_action.add_argument(
        "-t", "--benchmark",
        help="Ignore generated image, and print time rendering. "
             "Implies '-r' and '-e'.",
        dest='ACTION_ON_RESULT',
        default='view image',
        const='print time rendering',
        action='store_const'
    )

    run_group_result_action.add_argument(
        "-r", "--raw",
        help="Just run, don't process the results. Implies '-p'.",
        dest='ACTION_ON_RESULT',
        default='view image',
        const='send to stdout',
        action='store_const'
    )

    run_group_result_action.add_argument(
        "-p", "--no-view",
        help="Just generate the PNG, don't visualize it.",
        dest='ACTION_ON_RESULT',
        default='view image',
        const='generate png',
        action='store_const'
    )

    parser_run.add_argument(
        "-e", "--skip-build",
        help="Don't compile anything, even if the current executable is "
             "outdated.",
        action='store_false',
        dest='ALSO_BUILD_PROJ',
    )

    parser_run.add_argument(
        "-o", "--output",
        help=f"PNG output filename. Default: {default_png_filename} . "
             "Ignored if the PNG is not generated.",
        default=default_png_filename,
        dest='PNG_FILENAME',
    )

    parser_run.add_argument(
        "-i", "--no-overwrite",
        help="Never overwrite the output PNG file. Ignored if the PNG is "
             "not generated.",
        action='store_true',
        dest='NEVER_OVERWRITE_PNG',
    )

    parser_run.add_argument(
        "ARGS",
        help="Arguments to the executable.",
        nargs="*",
    )

    # DEBUG

    parser_debug = subparsers.add_parser('debug')
    parser_debug.set_defaults(func=debug_command)

    debug_group_build_type = parser_debug.add_mutually_exclusive_group()

    debug_group_build_type.add_argument(
        "-r", "--release",
        help="Debug release build (default is to debug the debug build).",
        dest='BUILD_TYPE',
        default='debug',
        const='release',
        action='store_const',
    )

    debug_group_build_type.add_argument(
        "-w", "--rel-with-deb",
        help="Debug 'Release with debug info' build (default is to debug "
             "the debug build).",
        dest='BUILD_TYPE',
        default='debug',
        const='relwithdebinfo',
        action='store_const',
    )

    parser_debug.add_argument(
        "-e", "--skip-build",
        help="Don't compile anything, even if the current executable is "
             "outdated.",
        action='store_false',
        dest='ALSO_BUILD_PROJ',
    )

    parser_debug.add_argument(
        "ARGS",
        help="Arguments to the executable.",
        nargs="*",
    )

    # ZHU LI,

    args = parser.parse_args()
    init()  # Inicializa as cores

    # DO THE THING

    if 'func' not in args:
        run_command(args)
    else:
        args.func(args)
