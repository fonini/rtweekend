#!/bin/bash

set -e

num_batches=7
batch_size=7

let N=num_batches*batch_size

date
./do-the-thing.py config -x
./do-the-thing.py build
for ((i=0;i<N;i++)); do
    printf "%03d " "$i"
    ./do-the-thing.py run -t -- -r 1080 2>/dev/null
done | tee "temp_times.numpy.txt"
date

printf "\n"

# force generation of current out.png with comment
png_filename="$(mktemp --suffix='.png')"
./do-the-thing.py run -epo "$png_filename" -- -r 144 >/dev/null 2>&1
epline="$(identify -format '%c' "$png_filename" |
          grep '^Executable path: ' --color=never)"
rm -f -- "$png_filename"
exa -lh "${epline#Executable path: }"

python -c '
import numpy as np
x = np.loadtxt("temp_times.numpy.txt")[:, 1]
x = x.reshape(('$batch_size', '$num_batches'))
x = x.min(axis=0)
print(f"{x.mean():.3g} +/- {x.std():.2g}")
'
