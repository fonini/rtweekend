#include <stdexcept>

#include <fmt/core.h>

#include "rtweekend.h"
#include "initialize_world.h"

#include "Ray.h"
#include "Color.h"
#include "HittableList.h"

#include "Camera.h"

constexpr Color sky_color_north{0.5, 0.7, 1.0};
constexpr Color sky_color_south{1.0, 1.0, 1.0};

Color Camera::ray_color(const Ray &r, const HittableList &world, int depth)
const
{
    if constexpr (build_type == BuildType::DEBUG)
        if (depth < 0)
            throw std::runtime_error("Unknown error: depth < 0");

    if (depth == 0)
        return Color{0, 0, 0};

    auto rec = world.hit(r, ray_start_nudge, infty);
    if (rec.hit()) {
        if constexpr (render_mode == RenderMode::REAL) {
            auto new_direction = rec.normal() + Vec3::random_inside_ball();
            return ray_absorption_factor * ray_color(
                {rec.p(), new_direction}, world, depth - 1);
        }
        else if constexpr (render_mode == RenderMode::NORMALS)
            // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers)
            return 0.5 * (rec.normal() + Color{1.0, 1.0, 1.0});
        else {  // HITCOUNT
            /* Obs: ao invés de Preto + 3 cores (no caso, preto + CMY),
             *      também poderíamos usar Preto + 4 cores (por exemplo,
             *      Preto + RGB + Branco), e nesse caso poderíamos gravar
             *      até 4 hits (ao invés de 3). Isso deve dar certo porque
             *      as médias entre 4 cores (i.e. combinações convexas)
             *      são pontos únicos do tetraedro RGBW, e portanto cada
             *      cor dentro deste tetraedro está associada a exatamente
             *      uma proporção de hitcounts. Não estamos fazendo isso
             *      porque, embora a interpretação ainda seja única, é uma
             *      interpretação mais difícil.
             */
            auto new_direction = rec.normal() + Vec3::random_inside_ball();
            auto further = ray_color(
                {rec.p(), new_direction}, world, depth - 1);
            if (further.x()==0 && further.y()==0 && further.z()==0)
                return Color{0, 1, 1};
            if (further.x()==0 && further.y()==1 && further.z()==1)
                return Color{1, 0, 1};
            if (further.x()==1 && further.y()==0 && further.z()==1)
                return Color{1, 1, 0};
        }
    }

    if constexpr (render_mode == RenderMode::HITCOUNT)
        return Color{0, 0, 0};

    auto unit_direction = r.direction().normalized();
    auto t = (unit_direction.y() + 1.0) / 2;
    return (1.0 - t)*sky_color_south + t*sky_color_north;
}

inline double normalized_coord(int i, int ii,
                               int subdivisions, int total_pixels) {
    // NOLINTNEXTLINE(cppcoreguidelines-avoid-magic-numbers)
    return (i + (ii + 0.5)/subdivisions) / total_pixels;
}

void Camera::render(
    const HittableList &world, int max_depth,
    FILE *out, FILE *progress) const
{
    fmt::print(out, "P3\n{} {}\n{}\n",
               image_width, image_height, COLOR_QUANTIZATION_LEVELS);
    const auto samples_per_pixel = antialiasing_factor*antialiasing_factor;
    const auto color_scale = 1.0 / samples_per_pixel;

    if constexpr (render_mode == RenderMode::HITCOUNT)
        max_depth = 3;

    for (auto j = image_height - 1; j >= 0; --j) {
        fmt::print(progress, "\r{} / {}", image_height - j, image_height);
        for (auto i = 0; i < image_width; ++i) {
            auto pixel_color = Color{0, 0, 0};
            for (auto jj = antialiasing_factor - 1; jj >= 0; --jj) {
            for (auto ii = 0; ii < antialiasing_factor; ++ii) {
                auto u = normalized_coord(
                    i, ii, antialiasing_factor, image_width);
                auto v = normalized_coord(
                    j, jj, antialiasing_factor, image_height);
                auto r = get_ray(u, v);
                pixel_color += ray_color(r, world, max_depth);
            }}
            pixel_color.normalize(color_scale);
            fmt::print(out, "{}", pixel_color);
        }
    }

    fmt::print(progress, "\nDone.\n");
}
