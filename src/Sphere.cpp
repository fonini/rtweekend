#include <limits>

#include "rtweekend.h"

#include "Sphere.h"

HitRecord Sphere::hit(const Ray &r, double t_min, double t_max) const {
    auto oc = r.origin() - center;
    auto dir_squared = r.direction().length_squared();
    auto dot_dir_oc = dot(r.direction(), oc);
    auto r2moc2 = radius*radius - oc.length_squared();
    auto d = dir_squared*r2moc2 + dot_dir_oc*dot_dir_oc;

    if (d < 0)
        return {false};
    auto sqrtd = std::sqrt(d);

    // Find the nearest root that lies in the acceptable range.
    auto root = - (dot_dir_oc + sqrtd) / dir_squared;
    if (root < t_min || root > t_max) {
        root = (sqrtd - dot_dir_oc) / dir_squared;
        if (root < t_min || root > t_max)
            return {false};
    }

    auto p_hit = r.at(root);

    if constexpr (RTOW_SUPER_ACCURATE_SPHERE) {
        // "Shadow acne": floating-point errors might make p_hit be found
        // *inside* the sphere. Nudge it outside the sphere.
        static constexpr auto nudge   = 1.0 + RTOW_SAS_MULTIPLIER*mach_eps;
        static constexpr auto denudge = 1.0 - RTOW_SAS_MULTIPLIER*mach_eps;
        double multiplier = nudge;
        if (r2moc2 > 0.0)  // if the ray originates from inside the sphere,
            multiplier = denudge;  // nudge *inside* instead of outside
        p_hit = center + multiplier*radius*normalized(p_hit - center);
    }

    return {r, root, p_hit, (p_hit - center)/radius};
}
