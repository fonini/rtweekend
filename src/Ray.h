#ifndef RAY_H
#define RAY_H

#include "Vec3.h"

class Ray {
    Point3 orig;
    Vec3 dir;

public:
    Ray(const Point3 &origin, const Vec3 &direction)
        : orig{origin}, dir{direction}
    {}

    Ray(const Point3 &origin, const Point3& direction)
        : Ray{origin, direction - origin}
    {}

    Point3 origin() const { return orig; }
    Vec3 direction() const { return dir; }

    Point3 at(double t) const {
        return orig + t*dir;
    }
};

#endif
