#ifndef RTWEEKEND_H
#define RTWEEKEND_H

#include <limits>
#include <memory>
#include <random>

// Assert floating point is IEEE 758 double precision
static_assert(std::numeric_limits<double>::is_iec559,
              "Floating point is not IEEE double, wtf?");

constexpr auto mach_eps = 0x1p-52;  // machine epsilon
constexpr auto sqrt_mach_eps = 0x1p-26;  // square root of machine epsilon
constexpr auto infty = std::numeric_limits<double>::infinity();
constexpr auto tau = 6.2831853071795864769252867665590057683943;

enum class BuildType {DEBUG, RELEASE};
#ifndef NDEBUG
    constexpr auto build_type = BuildType::DEBUG;
#else
    constexpr auto build_type = BuildType::RELEASE;
#endif

enum class RenderMode {REAL, NORMALS, HITCOUNT};
constexpr RenderMode render_mode = {RTOW_RENDER_MODE};

constexpr double ray_start_nudge =
        {RTOW_RAY_START_HOW_MUCH * RTOW_RAY_START_ORDER_OF_MAGN};

inline double random_double() {
    static std::uniform_real_distribution<double> distribution(0.0, 1.0);
    static std::default_random_engine generator;
    return distribution(generator);
}

inline double random_double(double min, double max) {
    return min + (max - min)*random_double();
}

#endif  // RTWEEKEND_H
