#ifndef HITTABLE_H
#define HITTABLE_H

#include <stdexcept>

#include "rtweekend.h"

#include "Vec3.h"
#include "Ray.h"

class HitRecord {
    bool _hit;
    bool _front_face;
    double _t;
    Point3 _p;
    Vec3 _normal;

public:
    /*  Converting constructor (i.e. non-explicit).
     *  Used to initialize a HitRecord that represents no hit at all. The
     *  member _t will represent the farthest 't' up to which a hit was
     *  searched for.
     */
    HitRecord(bool h, double t_far=infty)
        : _hit{false}, _t{t_far}
    {
    /* no need to initialize _front_face when _hit == false */
    // NOLINTNEXTLINE(clang-analyzer-optin.cplusplus.UninitializedObject)
        if constexpr (build_type == BuildType::DEBUG)
            if (h == true)
                throw std::invalid_argument(
                    "Missing HitRecord parameters.");
    }

    HitRecord(const Ray &r, double t_hit, const Point3 &p_hit,
              const Vec3 &outward_normal)
        : _hit{true},
          _front_face{dot(r.direction(), outward_normal) < 0},
          _t{t_hit}, _p{p_hit},
          _normal{_front_face ? outward_normal : -outward_normal}
    {}

    double t_hit() const { return _t; }
    bool hit() const { return _hit; }
    const Vec3 &normal() const { return _normal; }
    Point3 p() const { return _p; }
};

class Hittable {
public:
    virtual HitRecord hit(const Ray &r, double t_min, double t_max)
        const = 0;

    Hittable() = default;
    Hittable(const Hittable &) = delete;
    Hittable &operator=(const Hittable &) = delete;
    Hittable(Hittable &&) = default;
    Hittable &operator=(Hittable &&) = delete;

    virtual ~Hittable() = default;
};

#endif  // HITTABLE_H
