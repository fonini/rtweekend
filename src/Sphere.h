#ifndef SPHERE_H
#define SPHERE_H

#include "Hittable.h"
#include "Vec3.h"

class Sphere : public Hittable {
    Point3 center;
    double radius;

public:
    Sphere(const Point3 &c, double r) : center{c}, radius{r} {};
    HitRecord hit(const Ray &r, double t_min, double t_max) const override;
};

#endif  // SPHERE_H
