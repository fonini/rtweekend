#include <cstdio>

#include <unordered_map>

#include <fmt/core.h>
#include <fmt/color.h>

#include <CLI/App.hpp>
#include <CLI/Formatter.hpp>
#include <CLI/Config.hpp>
#include <CLI/Validators.hpp>

#include "rtweekend.h"
#include "initialize_world.h"

#include "Camera.h"


#define stringize_(s) #s
#define stringize(s) stringize_(s)


#define HEIGHT_PRESETS_STR "4320, 1080, 720, 360, or 144."
const std::unordered_map<int, int> resolution_presets = {
    {  144,   256},
    {  360,   640},
    {  720,  1280},  // HD
    { 1080,  1920},  // Full HD
    { 4320,  7680},  // UHD-2
};
constexpr auto ASPECT_RATIO = 16.0 / 9.0;


void dump_config(
    int image_height, double vp_height, double focal_len, int antialias,
    int max_depth
) {
    fmt::print("\nCMake vars:\n");

    fmt::print("    RTOW_RENDER_MODE:               {}\n",
        stringize(RTOW_RENDER_MODE_STR));

    fmt::print("    RTOW_DEFAULT_IMAGE_HEIGHT:      {}\n",
        stringize(RTOW_DEFAULT_IMAGE_HEIGHT));

    fmt::print("    RTOW_DEFAULT_VIEWPORT_HEIGHT:   {}\n",
        stringize(RTOW_DEFAULT_VIEWPORT_HEIGHT));

    fmt::print("    RTOW_DEFAULT_FOCAL_LENGTH:      {}\n",
        stringize(RTOW_DEFAULT_FOCAL_LENGTH));

    fmt::print("    RTOW_DEFAULT_ANTIALIAS:         {}\n",
        stringize(RTOW_DEFAULT_ANTIALIAS));

    fmt::print("    RTOW_DEFAULT_MAX_DEPTH:         {}\n",
        stringize(RTOW_DEFAULT_MAX_DEPTH));

    fmt::print("    RTOW_RAY_START_ORDER_OF_MAGN:   {}\n",
        stringize(RTOW_RAY_START_ORDER_OF_MAGN));

    fmt::print("    RTOW_SUPER_ACCURATE_SPHERE:     {}\n",
        RTOW_SUPER_ACCURATE_SPHERE ? "ON" : "OFF");

    fmt::print("    RTOW_SAS_MULTIPLIER:            {}\n",
        stringize(RTOW_SAS_MULTIPLIER));

    fmt::print("\nCommand line values:\n");

    fmt::print("    image_height:                   {}\n",
        image_height);

    fmt::print("    vp_height:                      {}\n",
        vp_height);

    fmt::print("    focal_len:                      {}\n",
        focal_len);

    fmt::print("    antialias:                      {}\n",
        antialias);

    fmt::print("    max_depth:                      {}\n",
        max_depth);
}


int main(int argc, char **argv) {

    constexpr const char *banner = build_type == BuildType::RELEASE ?
        "RTWeekend" : "RTWeekend (DEBUG Build)";

    // CLI parse

    auto app = CLI::App{banner};

    int image_height{RTOW_DEFAULT_IMAGE_HEIGHT};
    app.add_option(
        "-r,--res,--resolution", image_height,
        "Image height in pixels: " HEIGHT_PRESETS_STR
    );

    double focal_len{RTOW_DEFAULT_FOCAL_LENGTH};
    app.add_option(
        "-f,--focal-length", focal_len,
        "Distance from camera to viewport. (The viewport is fixed w.r.t. "
        "the scene objects.)"
    )->check(CLI::PositiveNumber);

    double vp_height{RTOW_DEFAULT_VIEWPORT_HEIGHT};
    app.add_option(
        "-p,--vp-height", vp_height,
        "Viewport height, in the same units as the focal length. Viewport "
        "width will be calculated as vp_height * aspect_ratio, which is "
        "fixed as 16:9."
    )->check(CLI::PositiveNumber);

    int antialias{RTOW_DEFAULT_ANTIALIAS};
    app.add_option(
        "-a,--antialias", antialias,
        "If this is N, then each pixel is subdivided into an NxN grid."
    )->check(CLI::PositiveNumber);

    int max_depth{RTOW_DEFAULT_MAX_DEPTH};
    app.add_option(
        "-d,--max-reflections", max_depth,
        "Maximum number of reflections considered for a given ray path."
    )->check(CLI::NonNegativeNumber);

    bool dump_config_bool = false;
    app.add_flag(
        "--dump-config", dump_config_bool,
        "Dump compilation flags and other values inferred from the "
        "command line, and exit."
    );

    CLI11_PARSE(app, argc, argv);

    fmt::print(stderr, fmt::emphasis::bold | fg(fmt::color::magenta),
               "{}\n", banner);

    // Image dimensions

    int image_width = -1;
    for (const auto &[key, value] : resolution_presets) {
        if (image_height == key) {
            image_width = value;
            break;
        }
    }
    if (image_width < 0) {
        fmt::print(stderr, "Unknown resolution: {}\n", image_height);
        return 1;
    }

    {
        auto computed_aspect_ratio =
            static_cast<double>(image_width)/image_height;
        if (computed_aspect_ratio != ASPECT_RATIO) {
            fmt::print(stderr,
                       "Aspect ratio should be 16:9\n"
                       "But it is: {}\n"
                       "This is an error!\n",
                       computed_aspect_ratio);
            return 1;
        }
    }

    if (dump_config_bool) {
        dump_config(
            image_height, vp_height, focal_len, antialias,
            max_depth
        );
        return 0;
    }

    auto camera = Camera{
        image_width, image_height,
        vp_height, focal_len,
        antialias
    };
    camera.render(get_world(), max_depth, stdout, stderr);

    return 0;

}
