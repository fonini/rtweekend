#ifndef INITIALIZE_WORLD_H
#define INITIALIZE_WORLD_H

#include "HittableList.h"

// multiplier for diffuse reflection
extern const double ray_absorption_factor;

HittableList get_world();

#endif  // INITIALIZE_WORLD_H
