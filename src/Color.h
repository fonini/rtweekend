#ifndef COLOR_H
#define COLOR_H

#include <cmath>

#include <fmt/core.h>
#include <fmt/format.h>

#include "rtweekend.h"

#include "Vec3.h"

constexpr int COLOR_QUANTIZATION_LEVELS = 256;
constexpr int COLOR_QUANTIZED_MAX = COLOR_QUANTIZATION_LEVELS - 1;

class Color : public Vec3 {
public:
    Color(const Vec3 &v) : Vec3{v} {}
    constexpr Color(double r, double g, double b) : Vec3{r, g, b} {}

    Color &operator+=(const Color &v) {
        (*this)[0] += v.x();
        (*this)[1] += v.y();
        (*this)[2] += v.z();
        return *this;
    }

    Color &operator/=(double t) {
        (*this)[0] /= t;
        (*this)[1] /= t;
        (*this)[2] /= t;
        return *this;
    }

    // Gamma correction with gamma=2
    void normalize(double scale) {
        using std::sqrt;
        (*this)[0] = sqrt(scale * (*this)[0]);
        (*this)[1] = sqrt(scale * (*this)[1]);
        (*this)[2] = sqrt(scale * (*this)[2]);
    }
};

inline Color operator/(Color c, double t) {
    return c /= t;
}

inline int quantize_color(double x) {
    // Convert x in [0, 1] to {0, 1, ..., 255}.
    // Narrowing conversion from long to int is ok because 'x' is in [0, 1]
    return static_cast<int>(std::lround(COLOR_QUANTIZED_MAX * x));
}

template <>
struct fmt::formatter<Color> {
    constexpr auto parse(format_parse_context& ctx) {
        auto it = ctx.begin();
        if constexpr (build_type == BuildType::DEBUG) {
            if (it != ctx.end()  &&  *it != '}')
                throw format_error("invalid format for Color");
        }
        return it;
    }

    template<typename FormatContext>
    auto format(const Color& c, FormatContext& ctx) {
        return format_to(ctx.out(), "{:d} {:d} {:d}\n",
                         quantize_color(c[0]),
                         quantize_color(c[1]),
                         quantize_color(c[2]));
    }
};

#endif
