#ifndef HITTABLE_LIST
#define HITTABLE_LIST

#include <vector>

#include "rtweekend.h"

#include "Hittable.h"

class HittableList : public Hittable {
public:
    using HittableHandle = std::shared_ptr<const Hittable>;

    void clear() { objects.clear(); }
    void add(const HittableHandle &obj) { objects.push_back(obj); }

    HitRecord hit(const Ray &r, double t_min, double t_max) const override;

private:
    std::vector<HittableHandle> objects;
};

#endif  // HITTABLE_LIST
