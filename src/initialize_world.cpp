#include "rtweekend.h"

#include "Sphere.h"
#include "HittableList.h"

#include "initialize_world.h"

const double ray_absorption_factor = 0.5;

HittableList get_world() {
    HittableList world;
    using std::make_shared;
    world.add(make_shared<Sphere>(Point3{0,    0,    0},     0.2));
    world.add(make_shared<Sphere>(Point3{0.4,  0,   -0.3},   0.2));
    world.add(make_shared<Sphere>(Point3{0, -100.2,  0},   100));
    return world;
}
