#ifndef VEC3_H
#define VEC3_H

#include "rtweekend.h"

//
//  Vec3
//

class Vec3 {
    double e[3];  // NOLINT(cppcoreguidelines-avoid-c-arrays)

public:
    Vec3() = default;  // allow uninitialized coordinates
    constexpr Vec3(double e0, double e1, double e2) : e{e0, e1, e2} {}

    constexpr double x() const { return e[0]; }
    constexpr double y() const { return e[1]; }
    constexpr double z() const { return e[2]; }

    constexpr Vec3 operator-() const { return {-e[0], -e[1], -e[2]}; }

    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index)
    constexpr double operator[](int i) const { return e[i]; }
    // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-constant-array-index)
    constexpr double &operator[](int i) { return e[i]; }

    constexpr Vec3 &operator+=(const Vec3 &v) {
        e[0] += v.e[0];
        e[1] += v.e[1];
        e[2] += v.e[2];
        return *this;
    }

    constexpr Vec3 &operator-=(const Vec3 &v) {
        e[0] -= v.e[0];
        e[1] -= v.e[1];
        e[2] -= v.e[2];
        return *this;
    }

    constexpr Vec3 &operator*=(const double t) {
        e[0] *= t;
        e[1] *= t;
        e[2] *= t;
        return *this;
    }

    constexpr Vec3 &operator/=(const double t) {
        return *this *= 1/t;
    }

    double length() const {
        return std::sqrt(length_squared());
    }

    constexpr double length_squared() const {
        return e[0]*e[0] + e[1]*e[1] + e[2]*e[2];
    }

    void normalize() {
        *this /= this->length();
    }

    Vec3 normalized() const {
        Vec3 n {*this};
        n.normalize();
        return n;
    }

    static Vec3 random_inside_ball(double radius=1) {
        Vec3 p;
        do p = {random_double(-1, 1),
                random_double(-1, 1),
                random_double(-1, 1)};
        while (p.length_squared() >= 1);
        return p *= radius;
    }
};

// vec3 Utility Functions

constexpr inline Vec3 operator+(Vec3 u, const Vec3 &v) {
    return u += v;
}

constexpr inline Vec3 operator-(Vec3 u, const Vec3 &v) {
    return u -= v;
}

constexpr inline Vec3 operator*(double t, Vec3 v) {
    return v *= t;
}

constexpr inline Vec3 operator/(Vec3 v, double t) {
    return v /= t;
}

constexpr inline double dot(const Vec3 &u, const Vec3 &v) {
    return u.x() * v.x()
         + u.y() * v.y()
         + u.z() * v.z();
}

constexpr inline Vec3 cross(const Vec3 &u, const Vec3 &v) {
    return {u.y() * v.z() - u.z() * v.y(),
            u.z() * v.x() - u.x() * v.z(),
            u.x() * v.y() - u.y() * v.x()};
}

inline Vec3 normalized(const Vec3 &v) {
    return v.normalized();
}


//
//  Point3
//

class Point3 : private Vec3 {
public:
    Point3() = default;
    constexpr Point3(const Vec3 &v) : Vec3{v} {}
    constexpr Point3(double e0, double e1, double e2) : Vec3{e0, e1, e2} {}

    constexpr Point3 &operator+=(const Vec3 &v) {
        (*this)[0] += v.x();
        (*this)[1] += v.y();
        (*this)[2] += v.z();
        return *this;
    }

    constexpr Point3 &operator-=(const Vec3 &v) {
        (*this)[0] -= v.x();
        (*this)[1] -= v.y();
        (*this)[2] -= v.z();
        return *this;
    }

    constexpr Vec3 &operator-=(const Point3 &v) {
        (*this)[0] -= v.x();
        (*this)[1] -= v.y();
        (*this)[2] -= v.z();
        return *this;
    }
};

constexpr inline Point3 operator+(Point3 u, const Vec3 &v) {
    return u += v;
}

constexpr inline Point3 operator-(Point3 u, const Vec3 &v) {
    return u -= v;
}

constexpr inline Vec3 operator-(Point3 u, const Point3 &v) {
    return u -= v;
}

#endif
