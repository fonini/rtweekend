#include "Hittable.h"

#include "HittableList.h"

HitRecord HittableList::hit(const Ray &r, double t_min, double t_max) const
{
    auto rec = HitRecord{false, t_max};
    for (const auto &obj : objects) {
        auto temp_rec = obj->hit(r, t_min, rec.t_hit());
        if (temp_rec.hit())
            rec = temp_rec;
    }
    return rec;
}
