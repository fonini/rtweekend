#ifndef CAMERA_H
#define CAMERA_H

#include "Vec3.h"
#include "Color.h"
#include "Ray.h"
#include "HittableList.h"

class Camera {
public:
    // image dimensions (in pixels)
    const int image_width, image_height;

    // distance from camera to viewport
    const double focal_length;

    // camera position
    const Point3 position;

    // viewport dimensions
    const double viewport_width,  viewport_height;

    // viewport edges
    const Vec3 horizontal, vertical;

    // viewport center
    const Point3 center;

    // viewport lower left corner
    const Point3 lower_left_corner;

    // square root of number of samples per pixel. works not only for
    // antialiasing but also for the rendering monte carlo.
    const int antialiasing_factor;

    Camera(int width, int height,
           double vp_height, double focal_len,
           int antialias)
        : image_width{width}, image_height{height},
          focal_length{focal_len}, position{0, 0, focal_length},
          viewport_width{vp_height*width/height},
          viewport_height{vp_height},
          horizontal{viewport_width, 0, 0},
          vertical{0, viewport_height, 0},
          center{0, 0 ,0},
          lower_left_corner{-viewport_width/2, -viewport_height/2, 0},
          antialiasing_factor{antialias}
    {}

    void render(const HittableList &world, int max_depth,
                FILE *out, FILE *progress) const;

private:
    Color ray_color(const Ray &r, const HittableList &world, int depth)
        const;

    Ray get_ray(double u, double v) const {
        return {position, lower_left_corner + u*horizontal + v*vertical};
    }
};

#endif
